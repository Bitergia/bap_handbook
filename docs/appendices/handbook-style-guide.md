Please use the following style guide when editing the Bitergia Handbook.


## No Main Title

The title is not needed. When mkdocs builds the html file it includes the title you set
in the TOC.


## Capitalize Titles

2nd and 3rd heading titles use the convention of starting each meaningful work (no
prepositions, pronouns, etc) with a capital letter. Titles always start with a capital
letter.

Example:

```markdown
## Second Level Title

### Third Level Title

#### Forth level title

##### Fifth level title
```


## Lists

Leave an empty line before list or it gets merged with the preceding text as a paragraph.

Leave two spaces at the beginning of each top-level list item. If the line is longer that
90 characters, start a new line two spaces after the `-` character.

Items lines should end with a period.

Sub-items need to be indented under the text of the previous item.

Bullet lists starts with a `-` character.

Example:

```markdown
  - Item A. Top level list item.
  - Item B. Break long items in several lines starting
    a new line like this
    one.
    - Item B.A. Sub-item indented under the item.
      - Item B.A.A. Another sub-item indented under the previous item.
    - Item B.B.
  - Item C.
    - Item C.A.
```

Numberd lists all start with `1.`

Example:

```markdown
  1. Item 1.
  1. Item 2.
     1. Item 2.1.
     1. Item 2.2.
     1. Item 2.3.
        1. Item 2.3.1.
        1. Item 2.3.2.
  1. Item 3.

```


## Line Limit is 90 Characters

Lines are limited to 90 characters, no white spaces at the end of the line. Only links
area allowed to have a larger length.

Example:

```markdown
Keys must be generated with the **RSA algorithm**, using at least **4096 bits**.
You can generate your own key by following the instructions on
[this post](https://www.vultr.com/docs/how-do-i-generate-ssh-keys/)
or on the 
[GitHub documentation](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-gpg-key).
```


## Bold Team Names

When referencing Bitergia Teams, bold their names. Optionally, link to their team page.

Example:

```markdown
**Sales team** or [Sales team](../teams/sales/sales-team.md).
```

## How to Include Images

Note that images have to be saved with the size and dimensions that you want to display
them.

To add an image, add an exclamation mark (!), followed by alt text in brackets, and the
path or URL to the image asset in parentheses. You can optionally add a title after the
URL in the parentheses.

```markdown
![Alt Text](images/image.ext "title")
```

  - _Alt Text:_ for screen readers and when image cannot be found.
  - _images/image.ext:_ location of image file; see below for more info.
  - _title:_ (OPTIONAL) is shown as tooltip when hovering with mouse over image.

### Location of Image Files

Images are stored in the _docs/assets/images/_ folder.
Within this folder is a structure that mimics the handbook.

  - _images/_: Add as many `../` as needed to get to the `assets/` folder

The name of the image file (`image.ext`) has two parts:

  - _image:_ name of the image itself.
  - _ext:_ file extension of the image type (e.g., .png, .jpg, .gif).

Example from the [Teams](../teams/teams/) page:

```markdown
![Organizational Chart picture](../assets/images/teams/bitergia-organizational-chart.png)
```


## End Section With Two New Lines

Leave a separation of two new lines between the start of the next section
and the previous one when a second level title starts. For the rest of them
as in third, fourth, fifth, and so on, leave one new line before they start.


## End Document With New Line

The last line of a document must be empty.


## Graphs

This handbook supports [thanks to a plugin](https://squidfunk.github.io/mkdocs-material/reference/diagrams/)
[mermaid diagrams](https://mermaid-js.github.io/mermaid/#/) like the following:

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```

Remember you have a live editor [here](https://mermaid.live/) with examples of the
[syntax of each diagram](https://mermaid-js.github.io/mermaid/#/examples)

