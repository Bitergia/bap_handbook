## Saving new dashboards or visualizations

Standard dashboards provided by Bitergia offer a lot of information, however, you may
wish to create your own visualizations or/and dashboards or edit some of the existing
ones. To do that, you just need to save it properly to avoid Bitergia overwriting
your work. It could happen if you store your work by overwriting some
standard dashboard or visualization.

The steps you should follow to ensure your work is safely saved are:

  - Click on `Edit`
  - Click on `Save`
  - Make sure `Save as new dashboard` or `Save as new visualization` toggle button is on.
  - Give your dashboard or visualization a new name that starts with `C_`, this
    convention will allow us to identify them as your _custom_ dashboard or
    visualization. For Dashboards, Bitergia ones always contain the word `Bitergia`
    within the description text.

![Save Dashboard](assets/images/custom_visualizations/save_dashboard.gif)


## Editing visualizations

  - [Login to Kibiter](../reporting_and_sharing#how-to-login).
  - Click on the `Edit` tab in the top right corner (gears should appear in the top
    right corner of each visualization).
  - Click on the `gear` icon of the target visualization.
  - Click on `Edit visualization`.
  - Follow the
    [saving new dashboards or visualizations](#saving-new-dashboards-or-visualizations)
    instructions if you are editing an existing dashboard/visualization not originally
    created by you. If you are modifying your own object, just overwrite it by clicking
    on the button `Save` in the top right corner.

![Save Visualization](assets/images/custom_visualizations/save_visualization.gif)


## Renaming visualizations

  - [Login to Kibiter](../reporting_and_sharing#how-to-login).
  - Click on the `Edit` tab in the top right corner (gears should appear in the top right
    corner of each visualization).
  - Click on the `gear` icon of the target visualization.
  - Click on `Customize panel` and change the title.
  - Once done, follow the
    [saving new dashboards or visualizations](#saving-new-dashboards-or-visualizations)
    instructions if you are editing an existing dashboard/visualization not originally
    created by you. If you are modifying your own object, just overwrite it by clicking
    on the button `Save` in the top right corner.

![Edit the name of a visualization](assets/images/custom_visualizations/rename_visualization.gif)


## Creating visualizations

  - [Login to Kibiter](../reporting_and_sharing#how-to-login).
  - Click on the `Edit` tab in the top right corner.
  - Click on `Create new`.
  - Select a visualization type.
  - Select a source (index pattern) to create your visualization on top of it.
  - Configure the visualization (metrics, buckets...).
  - Once you have finised you visualization, click on `Update` and then `Save`.
  - Type the name of the new visualization and click on `Save and return`. Remember to
    start the name of your custom visualization with `C_`.

![Create a new visualization](assets/images/custom_visualizations/create_visualization.gif)


## Editing a dashboard

  - [Login to Kibiter](../reporting_and_sharing#how-to-login).
  - Go to the target dashboard.
  - Click on the `Edit` tab in the top right corner.
  - Click on the `Add` tab in the top right corner.
  - Create a new visualization or load an existing one.
  - Once done, the visualization will be visible on the dashboard.
  - Move/resize the visualizations in the dashboard, set filters and time picker
    (optionally).
  - Once done, follow the
    [saving new dashboards or visualizations](#saving-new-dashboards-or-visualizations)
    instructions if you are editing an existing dashboard/visualization not originally
    created by you. If you are modifying your own object, just overwrite it by clicking
    on the button `Save` in the top right corner.

![Edit a dashboard](assets/images/custom_visualizations/edit_dashboard.gif)


## Creating dashboards

  - [Login to Kibiter](../reporting_and_sharing#how-to-login).
  - Open the left-side sidebar.
  - Go to the `Dashboard` section.
  - Click on the `Edit` tab in the top right corner.
  - Select `Add an existing` or create a new visualization.
  - Select the visualizations that you want to add.
  - Configure using the time picker the perior that you want to visualize.
  - Once done, click on the button `Save` on the top right corner.
  - Set the title and description of the new dashboard. Remember to start the name of
    your custom dashboard with `C_`.

![Create a new visualization](assets/images/custom_visualizations/create_dashboard.gif)
