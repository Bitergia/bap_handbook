## How to clear the kibana cache?

In order to clear the Kibana cache, you can rely on the functionalities provided by
your browser and delete the navigation history.

This can be useful when the landing page has changed.

  - For Firefox: [https://support.mozilla.org/en-US/kb/how-clear-firefox-cache](https://support.mozilla.org/en-US/kb/how-clear-firefox-cache)
  - For Chrome: [https://support.google.com/accounts/answer/32050?co=GENIE.Platform%3DDesktop&hl=en](https://support.google.com/accounts/answer/32050?co=GENIE.Platform%3DDesktop&hl=en)


## How to set custom time pickers?

In order to add custom time pickers, you can go to the section
`Time picker quick ranges` in `Management` -> `Advanced Settings` and add your
quickRanges to the list. An example of quickRange is provided below, as can be seen
`from` and `to` contain date values, `display` is the label shown in Kibiter, and
`section` defines the column where the quickRange will be added.

```json
{
  "from": "2020-03-21 00:00:00",
  "to": "2020-04-24 00:00:00",
  "display": "Release 0.2.40",
  "section": 3
}
```


## How to set a filter with painless code?

In some case it can be useful to define a filter which compares fields on the same
document. For instance, the following query allows you to get all documents having
the `grimoire_creation_date` value greater than `project_contribution_date`

```buildoutcfg
{
  "query": {
    "bool": {
      "filter": [
        {
          "script": {
            "script": {
              "source": "if (doc.containsKey('cm_contributed') && doc.containsKey('grimoire_creation_date')) { return doc['grimoire_creation_date'].value.getMillis() >= doc['cm_contributed'].value.getMillis()}",
              "lang": "painless"
            }
          }
        }
      ]
    }
  }
}
```

Such a query can be applied directly on a dashboard, by clicking on
`Add a filter` -> `Edit Query DSL`, and saving the query in the editor.

![Save painless filter](./assets/images/advanced/advanced_visualizations/save_painless_filter.png)


## How to use regular expressions?

You may want to filter by a data field using a pattern, for that purpose you will need
to use regular expressions, commonly called "regexp".

This is an example of a filter to show data filtered by the field `author_name` when
the two first characters are "Lu":

```json
{
  "query": {
    "regexp": {
      "author_name": "Lu.*"
    }
  }
}
```

Such query can be applied directly on a dashboard, by clicking on `Add a filter` ->
`Edit as Query DSL`, and saving the query in the editor.

In case you need more information about regular expressions, go to
the [documentation provided by OpenSearch](https://opensearch.org/docs/1.2/opensearch/query-dsl/term/#regex).


## How to import/export dashboards?

Import and export operations can be achieved via
[Kidash](https://github.com/chaoss/grimoirelab-kidash).

### Export

The dashboard with ID `Git` located at `http://localhost:9200` will be saved to
`./git.json`. If the flag `--split-index-patterns` is set, the index pattern will be
exported in the same folder where the `git.json` is saved, otherwise it will be
embedded in `git.json`.

```bash
kidash -g -e <Elasticsearch-url> --dashboard <dashboard-id>* --export <local-file-path>
ex.: kidash -g -e http://localhost:9200 --dashboard Git --export ./git.json --split-index-patterns
```

### Import

The dashboard included in `./git.json` will be uploaded to `http://localhost:9200`.
The same command can be used to import an index pattern.

```bash
kidash -g -e <Elasticsearch-url> --import <local-file-path>
ex.: kidash -g -e http://localhost:9200 --import ./git.json
```

## Data sources that not allow retrieving all their history

### mbox

The past conversations are not retrieved, we can collect only the conversations going
forward from the day when we started tracking the groups.

### slack

If you are using the free version, we can only retrieve the most recent 10,000 messages.
More info [here](https://slack.com/intl/en-es/help/articles/115002422943-Message-file-and-app-limits-on-the-free-version-of-Slack).

## Slack

### Getting the ID of Slack channel

Access your Slack instance via browser and click on the specific channel.

The URL in your browser will change to something similar to
`https://app.slack.com/client/TXXX/CYYY`.
`TXXX` is the team ID, while `CXXX` is the channel ID.

### Tracking a Slack instance

  - In your Slack instance, click the workspace name in the top left corner.
  - Select `invite people` from the menu, then click `Members`.
  - Enter the email address `owlbot@Bitergia.com`.
  - Under `Default Channels`, click `Edit/add` to choose the channels where
    `owlbot` will be added.
  - Click on `Send invitations`.
  - Once we receive the invitation, we will install the `OwlBot-app` in your workspace,
    and use its `OAuth Access Token`. The app will require the following scopes:
    - channels:history
    - channels:read
    - users:read

Depending on the workspace settings, the installation of the app may require the
admin's approval.

## Groups.io

The permission to download archives needs to be enabled. This is done by checking the
corresponding box within the `Admin>Settings>Message Policies` page. Note that if this
box is checked, any subscriber can download the archives.

## Adding a new MBox repository

In order to start tracking a MBox repository, it is needed to:

  - subscribe our bot `barnowl@Bitergia.com` to the target mailing list.
  - update the projects.json as follows:

    ```json
        "mbox": [
            "https://my-fantastic-group /home/Bitergia/mboxes/barnowl_my-fantastic-group",
            "https://my-amazing-group /home/Bitergia/mboxes/barnowl_my-amazing-group"
        ]
    ```

  - notify us opening an issue in our support platform (see
    [Support](../subscription/support#Support) )


??? faq "How to enable the tracking of a Slack instance?"
    You have to provide us permission.

    See more information [here](../subscription/setup_management#Tracking a Slack instance)

??? faq "How to get the ID of Slack channel?"
    The ID of a Slack channel is in the URL.

    See more information [here](../subscription/setup_management#Getting the ID of Slack channel)

??? faq "How to enable the download of archives in Groups.io?"
    You have to change the permissions.

    See more information [here](../subscription/setup_management#Tracking Groups.io)

??? faq "Which are the data sources that not allow retrieving all their history?"
    We have several.

    See more information [here](../subscription/setup_management#Data sources that not allow retrieving all their history)
