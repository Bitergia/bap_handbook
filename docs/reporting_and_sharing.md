## How to login

To perform some actions you will need to log in to your dashboard.
If your dashboard is private, you will get the login form once you enter your
dashboard (usually `yourcompany.biterg.io`)

![Sharing](assets/images/new_to_bap/login.gif)

If your dashboard is public, you have to log in by clicking on the top right corner
and then switching to your account.

## Sharing a dashboard

  - [Login to Kibiter](../reporting_and_sharing#how-to-login).
  - [Set a time interval and filters](../quick-start) (optionally).
  - Click on the `Share` tab in the top right corner.
  - A menu will drop down.
    - The first entry `>_ Embed code` allows to share the dashboard as an iFrame.
    - The second entry `Permalinks` allows sharing the dashboard as a permalink.

      In both cases, the URL generated can be either a snapshot or a saved object. The
      former encodes the current state of the dashboard (thus edits on the dashboard
      won't be visible via this URL), and the latter allows sharing the most recent
      version of the dashboard. Furthermore, the URL can be converted to a short link
      by enabling the `Short URL` toggle button.
  - Click on `Copy link/Copy iFrame code`.
  - Share the link!

![Sharing](assets/images/reporting_and_sharing/share_dashboard.gif)
