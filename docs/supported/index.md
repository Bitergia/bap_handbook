Bitergia Analytics collects data from 30+ different platforms to give you a
holistic picture of the software development projects that matter to you. This
section would give you a brief idea of what data can be fetched and analyzed
from the different data sources.

  - [GitHub](github.md)
  - [GitLab](gitlab.md)
  - [Git](git.md)
  - [Stack Overflow](stackoverflow.md)
  - [Jira](jira.md)
