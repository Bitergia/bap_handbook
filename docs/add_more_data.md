The data you can add to your instance can be basically classified in two buckets:

  - repositories: repositories, trackers, mailing lists, chat rooms, etc.
  - affiliations: contributor data (name, email, usernames), organizations (names and
    domains) and the relationship between contributors and organizations (enrollments).

In case you want to add more repositories to your Bitergia Analytics instance, we
recommend creating [a support ticket](../subscription/support/#support) with your request.
Bitergia has in its mid-term plan to create a user interface to add those repositories.
In the meantime, there is a way to take control of this by modifying a
[JSON file with the list of repositories for each project](../advanced/projects_json_file/).

The information about how to extend data from contributors or/and organizations is
available in the [affiliations](../advanced/affiliations/) section.
