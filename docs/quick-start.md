With this quick start you can learn the basic navigation inside the Bitergia Analytics
Platform in less than 2 minutes:

???+ info "Navigation"
    On the top bar, you have all your dashboards, generally sorted by
    [datasources](../supported/index#Index)
    ![Navigation](assets/images/new_to_bap/navigate.gif)

??? info "Date range"
    On the top right corner, you can filter the time that you want to analyze
    ![Date range](assets/images/new_to_bap/time.gif)

??? info "Item filter"
    A lot of visualizations allow filtering (in and out) by values you are interested in.
    ![Navigation](assets/images/new_to_bap/filters.gif)

??? tip "Pin filter"
    TIP: You can pin your filters across to use them in other dashboards
    ![Navigation](assets/images/new_to_bap/pin.gif)
