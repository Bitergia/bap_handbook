Welcome to the official documentation for Bitergia Analytics. If you want to
learn how to use it to get insights from software projects and the community,
you are in the right place.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9SxXxfE7ua8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Bitergia Analytics Platform Superpowers

Bitergia Analytics Platform delivers a holistic and insightful analytic picture of
software development projects to enhance performance and drive results.

Bitergia Analytics Platform has three superpowers to help you navigate the deep waters of
globally distributed software development processes.

  - It gathers data from over 30 sources to give you a holistic picture of the software 
    development projects that matter to you.
  - It features intelligent search engine options, from filtering bots to filling
    in gaps in data you have been searching.
  - It neatly visualizes your search results and lets you create super-charged dashboards
    with insights you want, with an option to share them with your boss, project mates, or
    across teams and organizations.

With the information and insights you discovered using our platform, you can evaluate the
health of projects, study trends, identify risks, compare projects, and advocate the work
that software development teams are doing.

We created Bitergia Analytics Platform to support your informed decision-making with peace
of mind that only relevant and well-timed data insights can give you. All for you to be
able to close the People Impact Gap and do your job with flying colors.


## But tell me, please, who does what, and how?

### Bitergia Analytics Platform clears up - Who? Does what? And how?

Our platform analyzes the project to identify who is doing what and how.

  - By **who**, we mean contributors, organizations, and companies.
  - By **what**, we mean the activity in the different tools used by the project
    contributors, such as GitHub, Stackoverflow, Jira, and other communication platforms,
    ticketing systems, and question-and-answer forums.  
  - By **how**, we mean the efficiency of the different processes, such as the time a
    community member waits until they get feedback on a pull request.

### Affiliations and identities (Who?)

Many organizations drive software development in modern communities, with teams being very
volatile. We can gather a lot of data, but it has many gaps about the contributors. Some
data sources lack helpful information like the employer. If the information is provided,
it is often outdated - the user filled it in once and didn't update it.

Our platform centrally manages the data for analysis, attaching the organization info for
each contributor (data enriching process). It also fills in the gaps allowing you to
identify the organizations contributors have worked for in the past years.

An added value is filtering bots so that you know who is a human contributor and who is
not.

### Tracking Activity (Does what?)

For instance, you can ask for

  - the reviewers' comments on a specific project;
  - the changes made in Javascript files in a specific directory by a particular company
    during a selected timeframe;
  - the increase or decrease of commits during the holidays;
  - the composition of the community as doers (committers) or talkers (issue authors).

### Measuring the performance based on the process analysis (Done How?)

Adopting a strategic approach to software development involves focusing on performance
measurements. Analytics is all about asking the right questions about
the development process.

Some examples of such questions are:

  - Is your team attending quickly to the petitions from volunteers?
  - Is your team able to reduce the backlog of open issues?
  - Is your team able to identify bottlenecks?
  - What happens when an internal or external community member creates the pull request?
  - How efficient is the review process?

### Visualizations for a complete understanding

Once the results are ready, Bitergia Analytics Platform visualizes them in charts or
plain metrics, enabling authorized users to get up-to-date information about employees'
and contractors' activities, the communities they form, and the development process
itself.


## What’s this metrics business all about?

Metrics measure what you want to measure, even when you want to measure something as
intangible as how good a software project development process is in theory and practice.

A metric is just a number, so for it to be meaningful and relevant, it has to be chosen
and defined in reference to your goal. If you hear three, it is meaningless to you, but
if we tell you that this is the number of children Prince William has, it starts to be
meaningful but still not relevant. It starts to be relevant when your boss asks you to
research who will be the heir to the British throne. Now we’re talking business.

There, it all starts with a goal. Why do you need the data analysis in the first place?
What will you do with the data analysis results? Of course, it is ok not to know where
you are heading and explore the data to develop some general insights and observations
for further specification. However, specifying your analysis goal supercharges your
insights capabilities that can guide specific actions to reach your goal faster.


## GQM is a field-tested metric definition method we use.

The GQM method (Goal-Question-Metric) is a field-tested method we use and recommend to
understand, measure, assess, and improve different aspects of software development
processes.

First, we have to define the following:

  - business, software development, or management goal (purpose) stated broadly for
    conducting the study;
  - the object of the study (community, process, or activity) to be improved or assessed;
  - the object of the study’s characteristics which can be translated into quantifiable
    metrics that, combined, will model the object of the study in a measurable way;
  - finally, targets (success criteria) for each of the metrics that will drive
    performance enhancement and better the overall health of the organization.


## Use Case of the GQM Method

Here is an example of a Use Case to present a GQM methodology.

  - Goal - Improve engagement within the open source project
  - Question 1. How many newly attracted developers are participating in the project?
    - M1. The number of newcomers.
    - M2. The relative number of newcomers out of the total number of developers.
  - Question 2. Is the attraction of new developers improving over time?
    - M1. The number of newcomers.
    - M2. The relative number of newcomers out of the total number of developers.
    - M3. The slope of the graph of newcomers coming to the project.
  - Question 3. Are we retaining the new developers?
    - M4. The number of developers still developing that entered during the last year.
    - M5. The relative number of retained developers out of the newcomers last year.


## Who will be the happiest using our Platform?

Bitergia Analytics Platform enables teams, specialists, and decision-makers to focus on
what they do best.

  - **Data Specialists** and **Engineering Leaders** can focus on connecting the dots,
    drawing insights from data, preparing visualizations, and customizing dashboards.
  - **Business and Governance Executive Officers** can make informed decisions about
    software development practices and culture based on visualizations and dashboards that
    show the impact of policy implementations.
  - **Open Source Community Managers** can gain better insights into the inner workings of
    a distributed open-source software community by analyzing what community members are
    doing, how they are working together, and where they do their work.
  - **DevRel Professionals** can drive their engagement strategy with insights into how
    developers engage in the community, which activities lead to more action, and when
    members are inactive.
  - **Open Source Foundation Leaders** can demonstrate their successful and fair
    stewardship of open source communities, identify new potential foundation members,
    provide transparency, and identify areas where open source communities need additional
    help.


## About Bitergia

Bitergia is the market leader in open software development analytics with 15+ years of
experience researching collaborative software development methodologies and quality
models.

Our Vision is that open-source collaboration tools and best practices will become the
industry standards for analyzing distributed software development projects. Our efforts
aim to lower the open source methodologies adoption barriers, enhance companies’ and
organizations’ performance, and drive desirable community changes.

We love free, libre, open-source software. Bitergia’s technology has always been 100%
free, open-source software. Bitergia supports events and initiatives related to free,
open-source software by sponsoring initiatives and participating as speakers or
attendees in various industry events.


## Our Success Stories

[Use cases and other resources](https://bitergia.com/resources).
