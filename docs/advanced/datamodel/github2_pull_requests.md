This index contains one document per GitHub pull request or a pull request
comment. It is possible to distinguish them by using the field `item_type`.

All the field types are aggregatable except the `text` field type. Those are:

  - `title_analyzed`.
  - `issue_title_analyzed`.
  - `body_analyzed`.

| **Name**                       | **Type**  | **Description**                                                                                                      |
|--------------------------------|-----------|----------------------------------------------------------------------------------------------------------------------|
| _Cross-references fields_      | NA        | Fields coming from cross-references study (available only when it is active), see cross_references.csv.              |
| assignee_geolocation           | geo_point | Pull request assignee geolocation from GitHub.                                                                       |
| author_bot                     | boolean   | True/False if the pull request author is a bot or not from the SortingHat profile.                                   |
| author_domain                  | keyword   | Pull request author domain name from SortingHat profile.                                                             |
| author_gender                  | keyword   | Pull request author gender, based on her name, from SortingHat (disabled by default).                                |
| author_gender_acc              | float     | Pull request author gender accuracy from SortingHat (disabled by default).                                           |
| author_id                      | keyword   | Pull request author ID from SortingHat profile.                                                                      |
| author_name                    | keyword   | Pull request author name from SortingHat profile.                                                                    |
| author_org_name                | keyword   | Pull request author organization name from SortingHat profile.                                                       |
| author_user_name               | keyword   | Pull request author username from SortingHat profile.                                                                |
| author_uuid                    | keyword   | Pull request author UUID from SortingHat profile.                                                                    |
| body                           | keyword   | Body of the pull request/comment.                                                                                    |
| body_analyzed                  | text      | Body of the pull request/comment.                                                                                    |
| code_merge_duration            | float     | Difference in days between creation and merging dates.                                                               |
| comment_created_at             | date      | Date when the comment was created.                                                                                   |
| comment_updated_at             | date      | Date when the comment was updated.                                                                                   |
| demography_max_date            | date      | Date of the latest pull request of the corresponding author. Available only when demography study is active.         |
| demography_min_date            | date      | Date of the first (oldest) pull request of the corresponding author. Available only when demography study is active. |
| forks                          | long      | Number of repository forks.                                                                                          |
| github_repo                    | keyword   | GitHub repository name.                                                                                              |
| grimoire_creation_date         | date      | Pull request/comment creation date.                                                                                  |
| id                             | keyword   | Pull request/comment ID.                                                                                             |
| is_github_pull_request         | long      | Used to separate pull requests from comments.                                                                        |
| is_github_review_comment       | long      | Used to separate pull requests from comments.                                                                        |
| is_github_comment              | long      | Used to unify pull requests and issue comments.                                                                      |
| item_type                      | keyword   | The type of the item (pull request/comment).                                                                         |
| merge_author_domain            | keyword   | Merge author domain from GitHub.                                                                                     |
| merge_author_geolocation       | geo_point | Merge author geolocation from GitHub.                                                                                |
| merge_author_location          | keyword   | Merge author location as string from GitHub.                                                                         |
| merge_author_login             | keyword   | Merge author login from GitHub.                                                                                      |
| merge_author_name              | keyword   | Merge author name from GitHub.                                                                                       |
| merge_author_org               | keyword   | Merge author organization from GitHub.                                                                               |
| merged_by_data_bot             | boolean   | True/False if the merge author is a bot or not, from SortingHat profile.                                             |
| merged_by_data_domain          | keyword   | Merge author domain from SortingHat profile.                                                                         |
| merged_by_data_gender          | keyword   | Merge author gender, based on her name, from SortingHat profile(disabled by default).                                |
| merged_by_data_gender_acc      | float     | Merge author gender accuracy from SortingHat profile(disabled by default).                                           |
| merged_by_data_id              | keyword   | Merge author's ID from SortingHat profile.                                                                           |
| merged_by_data_name            | keyword   | Merge author name from SortingHat profile.                                                                           |
| merged_by_data_org_name        | keyword   | Merge author organization from SortingHat profile.                                                                   |
| merged_by_data_user_name       | keyword   | Merge author username from SortingHat profile.                                                                       |
| merged_by_data_uuid            | keyword   | Merge author UUID from SortingHat profile.                                                                           |
| metadata__enriched_on          | date      | Date when the item was enriched.                                                                                     |
| metadata__gelk_backend_name    | keyword   | Name of the backend used to enrich information.                                                                      |
| metadata__gelk_version         | keyword   | Version of the backend used to enrich information.                                                                   |
| metadata__timestamp            | date      | Date when the item was stored in the RAW index.                                                                      |
| metadata__updated_on           | date      | Date when the item was updated on its original data source.                                                          |
| num_review_comments            | long      | Number of review comments.                                                                                           |
| origin                         | keyword   | Original URL where the repository was retrieved from.                                                                |
| project                        | keyword   | Project.                                                                                                             |
| project_1                      | keyword   | Project (if more than one level is allowed in the project hierarchy).                                                |
| pull_closed_at                 | date      | Date in which the Issue was closed.                                                                                  |
| pull_created_at                | date      | Date in which the Issue was created.                                                                                 |
| pull_id                        | keyword   | Pull request ID on GitHub.                                                                                           |
| pull_id_in_repo                | keyword   | Pull request ID in the GitHub repository.                                                                            |
| pull_labels                    | keyword   | Pull request assigned labels.                                                                                        |
| pull_merged                    | boolean   | True if the pull request was already merged.                                                                         |
| pull_merged_at                 | date      | Date when the pull request was merged.                                                                               |
| pull_state                     | keyword   | State of the pull request (open/closed/merged).                                                                      |
| pull_updated_at                | date      | Date when the pull request was last updated.                                                                         |
| pull_url                       | keyword   | Full URL of the pull request (empty for issue comment entries).                                                      |
| reaction_confused              | long      | Number of reactions 'confused'.                                                                                      |
| reaction_eyes                  | long      | Number of reactions 'eyes'.                                                                                          |
| reaction_heart                 | long      | Number of reactions 'heart'.                                                                                         |
| reaction_hooray                | long      | Number of reactions 'hooray'.                                                                                        |
| reaction_rocket                | long      | Number of reactions 'rocket'.                                                                                        |
| reaction_thumb_down            | long      | Number of reactions '-1'.                                                                                            |
| reaction_thumb_up              | long      | Number of reactions '+1'.                                                                                            |
| reaction_total_count           | long      | Number of total reactions.                                                                                           |
| repository                     | keyword   | Repository name.                                                                                                     |
| repository_labels              | keyword   | Custom repository labels defined by the user.                                                                        |
| review_state                   | keyword   | Review type APPROVED, COMMENTED, CHANGES_REQUESTED, or empty.                                                        |
| sub_type                       | keyword   | Type of the comment (pull review comment).                                                                           |
| tag                            | keyword   | Perceval tag.                                                                                                        |
| time_open_days                 | float     | Time the pull request is open counted in days.                                                                       |
| time_to_close_days             | float     | Time to close a pull request counted in days.                                                                        |
| time_to_merge_request_response | float     | Time to merge a pull request in days.                                                                                |
| issue_title                    | keyword   | The title of the pull request.                                                                                       |
| issue_title_analyzed           | text      | Pull request title split by terms to allow searching.                                                                |
| issue_id_in_repo               | keyword   | The issue's ID in the repository.                                                                                    |
| issue_url                      | keyword   | Full URL of the issue. Remember that this is also filled for pull requests because GitHub considered them as issues. |
| url                            | keyword   | Url of the pull request/comment.                                                                                     |
| user_data_bot                  | boolean   | True/False if the pull request author is a bot or not from the SortingHat profile.                                   |
| user_data_domain               | keyword   | Pull request author domain name from SortingHat profile.                                                             |
| user_data_gender               | keyword   | Pull Request author gender, based on her name, from SortingHat (disabled by default).                                |
| user_data_gender_acc           | float     | Pull request author gender accuracy from SortingHat (disabled by default).                                           |
| user_data_id                   | keyword   | Pull request author ID from SortingHat profile.                                                                      |
| user_data_name                 | keyword   | Pull request author name from SortingHat profile.                                                                    |
| user_data_org_name             | keyword   | Pull request author organization name from SortingHat profile.                                                       |
| user_data_user_name            | keyword   | Pull request author username from SortingHat profile.                                                                |
| user_data_uuid                 | keyword   | Pull request author UUID from SortingHat profile.                                                                    |
| user_domain                    | keyword   | Pull request author domain name from GitHub.                                                                         |
| user_geolocation               | geo_point | Pull request author geolocation from GitHub.                                                                         |
| user_location                  | keyword   | Pull request author location as string from GitHub.                                                                  |
| user_login                     | keyword   | Pull request author login from GitHub.                                                                               |
| user_name                      | keyword   | Pull request author username from GitHub.                                                                            |
| user_org                       | keyword   | Pull request author organization from GitHub.                                                                        |
| uuid                           | keyword   | Perceval UUID.                                                                                                       |
