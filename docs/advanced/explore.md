## Inspecting an index from Kibiter

An index can be queried via the `Discover` in Kibiter. To access the `Discover`, open
the left sidebar at the top left corner, just below the owl a click on `Discover`.

![Support](../assets/images/advanced/explore/discover.gif)

