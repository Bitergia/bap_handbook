This is the service chosen by customers who want to control all the data
workflow. Bitergia will deploy, maintain and update the Bitergia Analytics
platform in an infrastructure provided by the customer. In case customer's
policy is to avoid third party employees to get access to their infrastructure,
Bitergia's engineering team will teach how to deploy and maintain the platform
with the aim of allowing the customer to self maintain it.

Beyond the installation of the product, Bitergia can help you with the
following services:

  1. [Training sessions](./training.md) that will empower your team to be able
  to understand the most important metrics. At the end of the training, you will
  be able to create your own dashboards with the metrics most relevant to your
  team.
  1. [Consulting](./consultancy.md) services to solve advanced questions about
  data, customizations of the platform, and to work with your team in creating a
  metrics strategy.
  1. [Support](./support.md) to get help with using the product and resolve any issues.
  1. Customization of the product.

In case you are interested in this service do not hesitate to [contact us](https://bitergia.com/contact/).
