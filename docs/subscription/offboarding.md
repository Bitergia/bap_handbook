We thank you for the time you have been with us. When your contract expires we will
decommission and remove your support tracker and your Bitergia Analytics instance.
The data sets will be removed too in order to fulfill the [**GDPR**](https://gdpr.eu/) law.
In case you want to get a copy of your data sets before leaving, please let us know
so we can prepare everything.


## Procedure

  1. Open a support issue asking for the data.
  1. From the person who is going to get access to the data we need:
  ```default
    - The **Keybase username** to send the data safely.
    - a **GPG key** attached to his/her Keybase user. We'll use it to encrypt the data files.
    - a **SSH key** to give access to download the files. We can use GitLab/Github public
  key if you have it added.
  ```
  1. Bitergia generates the dump.
  1. When the data is ready you'll be provided with a username and an IP address.
  1. Use the **username to connect** to that server using SSH/SCP. You 'll need to use the
  private SSH key provided to us:
  ```bash
      ssh username@address -i /path/to/private_key
  ```
  1. The encrypted files will be at the user's home directory:
  ```bash
      -rw-r--r-- 1 username username 1.4G Mar  2 12:02 file_raw.tar.7z.gpg
      -rw-r--r-- 1 username username 6.0M Mar  2 13:36 file_identities.tar.7z.gpg
      -rw-r--r-- 1 username username 142M Mar  2 13:08 file_enriched.tar.7z.gpg
  ```
  1. After downloading the files, you should be able to decrypt them using your PGP key.
  From the host where you looged in to Keybase, run:
  ```bash
      keybase pgp decrypt -i file_raw.tar.7z.gpg -o file_raw.tar.7z
  ```
  1. Now you are able to extract the files from the 7z archive with any compression utility
  that supports 7-zip format.
  1. Inform Bitergia that you have been able to extract the files in order we can dispose
   or delete the data.


## Some help

### How to add the **GPG key** to your user in Keybase

```bash
   keybase pgp gen    # if you need a PGP key
   keybase pgp select # if you already have one in GPG
   keybase pgp import # to pull from stdin or a file
```

### How to add your **SSH key to your Github** account

You can read it on this [link](https://docs.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account).

### How to add your **SSH key to your GitLab** account

You have all the information [here](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account).
