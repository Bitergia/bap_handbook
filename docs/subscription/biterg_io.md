Your subscription to the Bitergia Analytics service includes:

  1. Access to the Bitergia Analytics platform, hosted by Bitergia, under a biterg.io subdomain,
  similar to the CHAOSS dashboard at [chaoss.biterg.io](https://chaoss.biterg.io).
  1. Public or private access to Bitergia Analytics dashboards for visual data querying,
  custom charts, and custom dashboards building.
  1. Private access to data produced by Bitergia Analytics through Elasticsearch
  API.
  1. [Training sessions](./training.md) that will empower your team to be able
  to understand the most important metrics. At the end of the training, you will
  be able to create your own dashboards with the metrics most relevant to your
  team.
  1. [Consulting](./consultancy.md) services to solve advanced questions about
  data, customizations of the platform, and to work with your team in creating a
  metrics strategy.
  1. [Support](./support.md) to get help with using the product and resolve any issues.

In case you are interested in this service, do not hesitate to [contact us](https://bitergia.com/contact/).
